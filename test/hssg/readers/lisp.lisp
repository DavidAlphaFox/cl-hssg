;;;; SPDX-License-Identifier AGPL-3.0-or-later

;;;; compound.lisp  Compound artifact implementation
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.(in-package #:hssg.artifact)
(defpackage #:hssg/test/reader/lisp
  (:use #:cl))
(in-package #:hssg/test/reader/lisp)

;; "Tests for the Lisp reader"
(clunit:defsuite hssg.reader.lisp (hssg/test:hssg))

(clunit:deffixture hssg.reader.lisp (@body)
  (let ((reader (hssg.reader:get-reader "lisp")))
    @body))

(clunit:deftest lisp-reader-is-registered (hssg.reader.lisp)
  "The Lisp reader is registered by default"
  (clunit:assert-eq #'hssg.reader.lisp::read-lisp-metadata reader reader))

(clunit:deftest lisp-reader-can-read-files (hssg.reader.lisp)
  "The Lisp reader can read the contents of a Lisp file"
  (let ((data (funcall reader "test/hssg/sample-files/metadata.lisp")))
    (hssg:let-metadata ((foo :foo)
                        (bar :bar)
                        (baz :baz))
        data
      (let ((clunit:*clunit-equality-test* #'string-equal))
        (clunit:assert-equality* "foo" foo foo)
        (clunit:assert-equality* "bar" bar bar)
        (clunit:assert-equality* "baz" baz baz)))))

(clunit:deftest lisp-reader-does-not-pollute (hssg.reader.lisp)
  "The Lisp reader does not leak symbols from the file"
  (funcall reader "test/hssg/sample-files/metadata.lisp")
  (clunit:assert-false (find-symbol "THROWAWAY-FUNCTION")))

;;;; SPDX-License-Identifier AGPL-3.0-or-later

;;;; verbatim.lisp  Varbatim artifact tests
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>
(defpackage #:hssg/test/artifact/verbatim
  (:use #:cl)
  (:import-from #:clunit #:defsuite #:deffixture #:deftest #:assert-equal)
  (:import-from #:hssg.artifact #:verbatim-artifact #:derive-artifact))
(in-package #:hssg/test/artifact/verbatim)

(clunit:defsuite hssg.artifact.verbatim (hssg/test:hssg))
(clunit:defsuite hssg.artifact.verbatim.constructor (hssg.artifact.verbatim))
(clunit:defsuite hssg.artifact.verbatim.deriving (hssg.artifact.verbatim))


;;; ---------------------------------------------------------------------------
(deftest verbatim-artifact-constructor (hssg.artifact.verbatim.constructor)
  (let ((artifact (hssg:make-verbatim-artifact #p"content/blog" #p"css/main.css")))
    (with-slots ((directory hssg.artifact::directory)
                 (file-name hssg.artifact::file-name))
        artifact
      (assert-equal #p"content/blog" directory)
      (assert-equal #p"css/main.css" file-name))))


;;; ---------------------------------------------------------------------------
(deftest derive-verbatim-artifact (hssg.artifact.verbatim.deriving)
  (let* ((artifact (hssg:make-verbatim-artifact #p"content/blog" #p"css/main.css"))
         (instruction (derive-artifact artifact)))
    (with-slots ((base-path hssg.filesystem::base-path)
                 (path hssg.filesystem::path))
        instruction
      (assert-equal #p"content/blog/" base-path)
      (assert-equal #p"css/main.css" path))))

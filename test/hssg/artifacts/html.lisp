;;;; SPDX-License-Identifier AGPL-3.0-or-later

;;;; html.lisp  HTML artifact tests
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>
(defpackage #:hssg/test/artifact/html
  (:use #:cl)
  (:import-from #:clunit #:defsuite #:deffixture #:deftest #:assert-equal #:assert-true)
  (:import-from #:hssg.artifact #:html-artifact #:derive-artifact))
(in-package #:hssg/test/artifact/html)

(defsuite hssg.artifact.html (hssg/test:hssg))

(hssg:deftemplate dummy-template (content)
  (:content
    `(:html
       (:head)
       (:body
         ,@content))))


;;; ---------------------------------------------------------------------------
(deftest derive-html-artifact (hssg.artifact.html)
  (let* ((artifact (make-instance 'hssg:html-artifact
                     :data '((:content . ((:p "Hello world!"))))
                     :template #'dummy-template
                     :output #p"blog/index.html"))
         (instruction (derive-artifact artifact)))
    (assert-true (typep instruction 'hssg:write-string-contents))
    (with-slots ((contents hssg.filesystem::contents)
                 (path     hssg.filesystem::path))
        instruction
      (assert-equal "<!DOCTYPE html>
<html><head></head><body><p>Hello world!</p></body></html>"
                    contents)
      (assert-equal #p"blog/index.html" path))))

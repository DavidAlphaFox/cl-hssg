;;;; SPDX-License-Identifier AGPL-3.0-or-later

;;;; directory.lisp  Varbatim artifact tests
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>
(defpackage #:hssg/test/artifact/directory
  (:use #:cl)
  (:import-from #:clunit #:defsuite #:deffixture #:deftest #:assert-equal #:assert-true)
  (:import-from #:hssg.artifact #:directory-artifact #:derive-artifact))
(in-package #:hssg/test/artifact/directory)

(clunit:defsuite hssg.artifact.directory (hssg/test:hssg))


;;; ---------------------------------------------------------------------------
(deftest derive-directory-artifact (hssg.artifact.directory)
  (let* ((artifact (hssg:make-directory-artifact #p"content/blog" #p"assets/images"))
         (instruction (hssg.artifact:derive-artifact artifact)))
    (assert-true (typep instruction 'hssg:copy-directory))
    (with-slots ((base-path hssg.filesystem::base-path)
                 (path      hssg.filesystem::path))
        instruction
      (assert-equal #p"content/blog/" base-path)
      (assert-equal #p"assets/images/" path))))

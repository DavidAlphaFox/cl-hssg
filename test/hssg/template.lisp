;;;; SPDX-License-Identifier AGPL-3.0-or-later

;;;; compound.lisp  Compound artifact implementation
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.(in-package #:hssg.artifact)
(defpackage #:hssg/test.template
  (:use :cl))
(in-package #:hssg/test.template)

;; "Tests for the HTML template engine"
(clunit:defsuite hssg.template (hssg/test:hssg))

(clunit:deffixture hssg.template (@body)
  "Default dummy data to operate on"
  (let ((foo-data '((:foo . "foo")))
        (foo-bar-data '((:foo . "foo")
                        (:bar . "bar"))))
    @body))


(clunit:deftest let-metadata-form (hssg.template)
  "The LET-METADATA form can bind data from an association list"
  (hssg:let-metadata ((foo :foo)
                      (bar :bar)
                      (baz :baz)
                      (qux :qux "qux"))
      foo-bar-data
    (clunit:assert-equal "foo" foo foo)
    (clunit:assert-equal "bar" bar bar)
    (clunit:assert-equal   nil baz baz)
    (clunit:assert-equal "qux" qux qux)))

(clunit:deftest identity-template-unchanged (hssg.template)
  "The identity template returns its input unchanged"
  (let ((out  (hssg:apply-template 'hssg:identity-template foo-bar-data)))
    (clunit:assert-equal foo-bar-data out out)))

(clunit:deftest identity-template-eq (hssg.template)
  "Applying the identity template does not produce a new object"
  (let ((out  (hssg:apply-template 'hssg:identity-template foo-bar-data)))
    (clunit:assert-eq foo-bar-data out out)))

(clunit:deftest anonymous-template (hssg.template)
  "Defines an anonous template"
  (let* ((template (hssg:template (foo)
                     (:foo (string-upcase foo))
                     (:baz "baz")))
         (out (hssg:apply-template template foo-bar-data)))
    (hssg:let-metadata ((foo :foo)
                        (bar :bar)
                        (baz :baz))
        out
      (let ((clunit:*clunit-equality-test* #'string-equal))
        (clunit:assert-equality* "FOO" foo foo)
        (clunit:assert-equality* "bar" bar bar)
        (clunit:assert-equality* "baz" baz baz)))))

(clunit:deftest template-chaining (hssg.template)
  "Chaining two templates produces a new template"
  (let* ((t1 (hssg:template ()
               (:bar "bar")))
         (t2 (hssg:template (foo)
               (:foo (string-upcase foo))
               (:baz "baz")))
         (t3 (hssg:template (bar)
               (:bar (string-upcase bar))))
         (template (hssg:chain-templates t1 t2 t3))
         (out (hssg:apply-template template foo-data)))
    (hssg:let-metadata ((foo :foo)
                        (bar :bar)
                        (baz :baz))
        out
      (let ((clunit:*clunit-equality-test* #'string-equal))
        (clunit:assert-equality* "FOO" foo foo)
        (clunit:assert-equality* "bar" bar bar)
        (clunit:assert-equality* "baz" baz baz)))))

(clunit:deftest initial-data (hssg.template)
  "Providing a tamplate with initial data produces a new template"
  (let* ((template (hssg:template-with-data (hssg:template () (:baz "baz"))
                                            '((:bar . "bar"))))
         (out (hssg:apply-template template foo-data)))
    (hssg:let-metadata ((foo :foo)
                        (bar :bar)
                        (baz :baz))
        out
      (let ((clunit:*clunit-equality-test* #'string-equal))
        (clunit:assert-equality* "foo" foo foo)
        (clunit:assert-equality* "bar" bar bar)
        (clunit:assert-equality* "baz" baz baz)))))

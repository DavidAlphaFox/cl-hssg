;;; This file produces HSSG metadata when read by a Lisp reader
(defun throwaway-function ()
  "This function must not leak out of this file"
  "foo")

`((:foo . ,(throwaway-function))
  (:bar . "bar")
  (:baz . "baz"))

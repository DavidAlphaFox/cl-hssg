.. default-role:: code

###################
 Things to be done
###################


Core
####

New feature: sources
====================

Currently the way we get data into HSSG is a mess without any consistency. I
propose the idea of "sources", "processors" and "sinks".

- A source is some object which represents the initial point of data
- A processor is some object which transforms data into other data
- A sink is the final destination of the data which produces the actual output
  file

We already have artifacts as sinks and templates as filters. This only leaves
sources to be implemented. A source is some object which implements the source
protocol:

- `GET-DATA`: Returns the data for further processing

It is up to the source to implement for example some form of caching. A source
could read data from a file, it could be hard-coded data, or it could use OCR
to parse a message delivered by carrier pigeon.


Cleanup
=======

- A proper public interface to the various artifact classes
- Expose reader interface to public
- Remove `WRITE-ARTIFACT` generic function and its methods once file system and
  instructions are implemented everywhere

Testing
=======

- Update reader tests to public interface once it is done


Blog
####

Fixes
=====

- Make sure the `lisp` reader uses throwaway packages instead of polluting the
  global namespace

Features
========

- Template: docstrings in template
- Localization: Fall back to English if a particular key does not exist for the
  target language

Cleanup
=======

- How much of each artifact's internals need to be exposed? Make accessor
  functions? Constructor functions over `MAKE-INSTANCE`?
- Static page metadata without binding (see comment in definition)

Testing
=======

Everything


CommonMark reader
#################

Testing
=======

Everything

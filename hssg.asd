;;;; SPDX-License-Identifier AGPL-3.0-or-later

;;;; hssg.asd  Hackable Static Site Generator system definition
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.

(asdf:defsystem #:hssg
  :description "A hackable static site generator for Common Lisp."
  :author "HiPhish <hiphish@posteo.de>"
  :license "AGPL-3.0-or-later"
  :version "0.0.0"
  :depends-on ("alexandria" "cl-fad" "uiop" "local-time" "plump")
  :serial t
  :components ((:module "src"
                :components ((:module "hssg"
                              :components ((:file "package")
                                           (:file "config")
                                           (:file "filesystem")
                                           (:file "reader")
                                           (:module "readers"
                                            :components ((:file "lisp")))
                                           (:file "artifact")
                                           (:module "artifacts"
                                            :components ((:file "classes")
                                                         (:file "compound")
                                                         (:file "html")
                                                         (:file "xml")
                                                         (:file "verbatim")
                                                         (:file "directory")))
                                           (:file "template"))))))
  :in-order-to ((test-op (test-op "hssg/test"))))

(asdf:defsystem #:hssg/test
  :description "Tests for HSSG"
  :author "HiPhish <hiphish@posteo.de>"
  :license "AGPL-3.0-or-later"
  :version "0.0.0"
  :depends-on ("hssg" "clunit2" "uiop")
  :serial t
  :perform (test-op (o s)
             (symbol-call :hssg/test :test-all))
  :components ((:module "test"
                :components ((:file "mocking")
                             (:module "hssg"
                              :components ((:file "package")
                                           (:file "main")
                                           (:file "file-system")
                                           (:file "template")
                                           (:file "reader")
                                           (:module "readers"
                                            :components ((:file "lisp")))
                                           (:module "artifacts"
                                            :components ((:file "compound")
                                                         (:file "directory")
                                                         (:file "html")
                                                         (:file "verbatim")
                                                         (:file "xml")))))))))

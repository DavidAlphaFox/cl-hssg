.. default-role:: code


################################################
 Hackable static site generator for Common Lisp
################################################

This is a static site generator. It is hackable. And complicated.

In HSSG a website it not an HSSG project, it is a regular Common Lisp system
and HSSG acts as a library. This gives website projects full freedom in how
they want to build and structure their website.

The HSSG system itself only provides the bare minimum of a useful static site
generator core. Additional higher-level features, such as a blog or a gallery,
can then be built on top of the core. These plugins are just regular Common
Lisp systems with their own dependencies, and a website project can depend on
them like on any other system.


Status of the project
#####################

Things will change. Do not use in production unless you are willing to follow
`HEAD`. In fact, you probably should not be using it at all. How did you even
find it? Right now I am dogfooding my own product.


Documentation
#############

See the `HACKING`_ file for a rough overview of the project.


License
#######

Release under the GNU Affero General Public License v3.0 or later (SPDX_
license identifier `AGPL-3.0-or-later`). Refer to the LICENSE_ file for
details.


.. ----------------------------------------------------------------------------
.. _HACKING: HACKING.rst
.. _LICENSE: LICENSE
.. _SPDX: https://spdx.dev/

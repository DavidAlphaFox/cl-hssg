;;;; SPDX-License-Identifier AGPL-3.0-or-later

;;;; hssg-blog-cmark.asd  CommonMark reader for blog content
;;;; definition Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG-BLOG.
;;;;
;;;; CL-HSSG-BLOG is free software: you can redistribute it and/or modify it
;;;; under the terms of the GNU Affero General Public License as published by
;;;; the Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG-BLOG is distributed in the hope that it will be useful, but
;;;; WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
;;;; or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public
;;;; License for more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG-BLOG  If not, see <https://www.gnu.org/licenses/>.

(asdf:defsystem #:hssg-blog-cmark
  :description "Blog extension to HSSG, CommonMark reader"
  :author "HiPhish <hiphish@posteo.de>"
  :license  "AGPL-3.0-or-later"
  :version "0.0.0"
  :depends-on ("cmark" "cl-ppcre")
  :serial t
  :components ((module "src"
                :components ((:module "cmark-reader"
                              :components ((:file "main")))))))

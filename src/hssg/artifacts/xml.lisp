;;;; SPDX-License-Identifier AGPL-3.0-or-later

;;;; xml  XML artifact implementation
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.
(in-package #:hssg.artifact.xml)

(defmethod hssg.artifact:derive-artifact ((artifact hssg.artifact:xml-artifact))
  (with-slots ((data hssg.artifact::data)
               (output hssg.artifact::output))
      artifact
    (make-instance 'hssg.filesystem:write-string-contents
                   :contents (plump:serialize (sexp->xml-tree data) nil)
                   :path output)))

(defun sexp->xml-tree (sexp &key (version "1.0") (encoding "UTF-8"))
  "Converts one s-expression tree to an XML document tree. All the neccessary
  attributes of the XML header node will set from the (optional) keyword
  arguments ."
  (let* ((result (plump-dom:make-root (plump:make-child-array 2)))
         (header (plump:make-xml-header result)))
    (plump:set-attribute header "version" version)
    (plump:set-attribute header "encoding" encoding)
    (hssg.artifact.html::sexp->plump-tree sexp result)  ; Called for side effect only
    result))

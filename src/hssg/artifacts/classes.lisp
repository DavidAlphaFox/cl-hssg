;;;; SPDX-License-Identifier AGPL-3.0-or-later

;;;; classes.lisp  Artifact class definitions
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.
(in-package #:hssg.artifact)

(defclass verbatim-artifact ()
  ((file-name :initarg :file :type pathname
              :documentation "Original file name")
   (directory :initarg :directory :type pathname
         :documentation "Base path to the file to copy, will not be copied"))
  (:documentation "Artifact which copies one filed to another location"))

(defclass directory-artifact ()
  ((directory :initarg :directory :type pathname
              :documentation "Original directory path, relative to base directory")
   (base :initarg :base :type pathname
         :documentation "Base path to the directory, will not be copied"))
  (:documentation "Artifact which copies a directory and its contents recursively to another location"))

(defclass compound-artifact ()
  ((artifacts :initarg :artifacts :initform '() :type list
              :documentation "List of wrapped artifacts"))
  (:documentation
   "A higher-order artifact which contains any number of other artifacts."))

(defclass html-artifact ()
  ((data :initarg :data :type list
         :documentation "Alist of SXML trees of the page data; does not have to be
serializable by Spinneret initially, but needs to be eventually.")
   (template :reader artifact-template :initarg :template :type function
             :documentation "Template to apply to the data")
   (output :reader artifact-output :initarg :output :type pathname
           :documentation "Path where to write the produced artifact."))
  (:documentation "Will produce an HTML file."))

(defclass xml-artifact ()
  ((data :initarg :data :type list
         :documentation "SXML tree of the XML file in PITHY-XML format.")
   (output :initarg :output :type pathname
           :documentation "Path where to write the produced artifact."))
  (:documentation "Will produce an XML file."))

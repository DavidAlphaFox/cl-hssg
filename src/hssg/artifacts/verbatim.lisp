;;;; SPDX-License-Identifier AGPL-3.0-or-later

;;;; verbatim.lisp  Verbatim artifact implementation
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.
(in-package #:hssg.artifact.verbatim)

;;; Maybe this could be a public config parameter? It could be useful for other
;;; file copy operations as well.
(defparameter *buffer-size* 512
  "Buffer size to use when copying a file. The file will be copied in chunks of
  this size.")

(defmethod hssg.artifact:derive-artifact ((artifact hssg.artifact:verbatim-artifact))
  (with-slots ((file-name hssg.artifact::file-name)
               (directory hssg.artifact::directory))
      artifact
    (make-instance 'hssg.filesystem:copy-file
                   :base-path (fad:pathname-as-directory directory)
                   :path      (fad:pathname-as-file file-name))))

(defun make-verbatim-artifact (base-dir file-name)
  "Constructor for a verbatim file artifact which copies to contents of
  FILE-PATH, relative to INPUT-DIR to the same path relative to OUTPUT-DIR."
  (declare (type (or string pathname) base-dir file-name))
  (make-instance 'hssg.artifact:verbatim-artifact :directory base-dir :file file-name))

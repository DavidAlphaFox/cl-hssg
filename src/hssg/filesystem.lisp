;;;; SPDX-License-Identifier AGPL-3.0-or-later

;;;; filesystem.lisp  File system abstraction layer
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.(in-package #:hssg.artifact)
(in-package #:hssg.filesystem)


;;; --- ABSTRACT CLASS DEFINITIONS --------------------------------------------
(defclass file-system ()
  ()
  (:documentation "Abstract base class of all file systems."))

(defclass file-system-instruction ()
  ()
  (:documentation "Abstract base class of all file system instructions"))

(defclass path-instruction (file-system-instruction)
  ((path :initarg :path :reader instruction-path :type pathname
         :documentation "Path to the output file"))
  (:documentation "An instruction which accesses exactly one file."))


;;; --- PROTOCOL --------------------------------------------------------------
(defgeneric write-to-filesystem (instruction file-system)
 (:documentation
  "Applies the given INSTRUCTION to the given FILE-SYSTEM, for side effects."))


;;; --- FILE SYSTEM CLASSES ---------------------------------------------------
(defclass base-file-system (file-system)
  ((directory :initarg :directory :reader file-system-directory :type pathname
     :documentation "Actual directory within the file system."))
  (:documentation
   "A file system which accesses files of the host OS relative to a given base
directory."))

(defclass overlay-file-system (file-system)
  ((directory :initarg :directory :type pathname
     :documentation "Directory of the file system relative to parent.")
   (parent :initarg :parent :type (or overlay-file-system base-file-system)
     :documentation "Parent file system."))
  (:documentation
   "File system whose operations are all relative to a parent file system."))


;;; --- INSTRUCTION CLASSES ---------------------------------------------------
(defclass write-string-contents (path-instruction)
  ((contents :initarg :contents :initform (make-string 0) :type string
     :documentation "The file content as a string"))
  (:documentation
   "Instruction which creates a file with the given content."))

(defclass copy-file (path-instruction)
  ((base-path :initarg :base-path :type pathname
     :documentation "Path to the directory containing the file."))
  (:documentation
   "Instruction which copies a given file varbatim. The file will be copied
from BASE-PATH/PATH to PATH. The file system is responsible to prepending a new
base path to the output file."))

(defclass copy-directory (path-instruction)
  ((base-path :initarg :base-path :type pathname
              :documentation "Path to the directory."))
  (:documentation
   "Instruction which copies a given directory recursively. The directory will
be copied from BASE-PATH/PATH to PATH. The file system is responsible for
prepending a new base path to the output file."))


(defclass compound-instruction (file-system-instruction)
  ((instructions :initarg :instructions :initform (list) :type list
     :documentation "List of instrunctions to perform."))
  (:documentation "Container instruction which wraps several instructions."))


;;; --- HELPER FUNCTIONS ------------------------------------------------------
;;; These functions encapsulate the lowest possible level of abstraction away
;;; from the actual file system I/O. They should be mocked during tests of
;;; dependent code, and tested individually on their own (even though they are
;;; not part of the public package interface).
(defun copy-file (from to) 
  "Copies the file at FROM to location TO, overwriting it if necessary."
  (declare (type pathname from to))
  (ensure-directories-exist to)
  (fad:copy-file from to :overwrite t)
  nil)

(defun copy-directory (from to) 
  "Recursively copies the contents of FROM to directory TO, overwriting it if
  necessary."
  (uiop/filesystem:collect-sub*directories
    from
    (lambda (directory) (declare (ignore directory)) t)
    (lambda (directory) (declare (ignore directory)) t)
    (lambda (directory)
      (declare (type pathname directory))
      (dolist (file (uiop/filesystem:directory-files directory))
        (let* ((relative (enough-namestring file (uiop/pathname:merge-pathnames* from)))
               (target   (merge-pathnames relative to)))
          (ensure-directories-exist target)
          (uiop:copy-file file target)))))
  nil)

(defun write-string-to-file (contents path) 
  "Writes the string CONTENTS to the file at PATH, creating it if necessary."
  (declare (type string contents)
           (type pathname path))
  (ensure-directories-exist path)
  (with-open-file (out path :direction :output :if-exists :supersede
                            :if-does-not-exist :create)
    (write-string contents out))
  nil)

(defun file-system-path (filesystem)
  "The absolute path of a file system, either a BASE-FILE-SYSTEM or an
  OVERLAY-FILE-SYSTEM. Multiple overlay file systems can be nested on top of
  another."
  (declare (type (or base-file-system overlay-file-system) filesystem))
  (etypecase filesystem
    (base-file-system
      (fad:pathname-as-directory (slot-value filesystem 'directory)))
    (overlay-file-system
      (with-slots (directory parent) filesystem
        (fad:merge-pathnames-as-directory
          (file-system-path parent)
          (fad:pathname-as-directory directory))))))


;;; --- IMPLEMENTATIONS -------------------------------------------------------
(defmethod write-to-filesystem ((instruction write-string-contents)
                                (file-system base-file-system))
  "A primitive implementation producing one file for fixed contents and an
  absolute file system."
  (let ((path (fad:merge-pathnames-as-file
                (fad:pathname-as-directory (file-system-directory file-system))
                (instruction-path instruction))))
    (with-slots (contents) instruction
      (write-string-to-file contents path))))

(defmethod write-to-filesystem ((instruction copy-file)
                                (file-system base-file-system))
  "Copies a file verbatim relative to a base directory."
  (with-slots (path base-path file) instruction
    (with-slots (directory) file-system
      (let ((original (fad:merge-pathnames-as-file
                        (fad:pathname-as-directory base-path) path))
            (copy     (fad:merge-pathnames-as-file
                        (fad:pathname-as-directory directory) path)))
        (copy-file original copy)))))

(defmethod write-to-filesystem ((instruction copy-directory)
                                (file-system base-file-system))
  "Copies a directory recursively to a base directory."
  ;; NOTE: This could have been implemented by walking over the source
  ;; directory and creating a COPY-FILE instruction for each file, but I
  ;; decided copy the directory directly instead. This is more efficient
  ;; because it does not create a number of intermediate instructions, and it
  ;; is easier to test because the function which accesses the physical file
  ;; system is isolated and can be mocked.
  (with-slots (path base-path) instruction
    (with-slots (directory) file-system
      (let ((from (fad:merge-pathnames-as-directory
                    (fad:pathname-as-directory base-path)
                    (fad:pathname-as-directory path)))
            (to   (fad:merge-pathnames-as-directory
                    (fad:pathname-as-directory directory)
                    (fad:pathname-as-directory path))))
        (copy-directory from to)))))

(defmethod write-to-filesystem (instruction
                                (file-system overlay-file-system))
  "Reduces the overlay file system first, then applies instructions to the
  reduced one."
  (let ((directory (file-system-path file-system)))
    (write-to-filesystem
      instruction
      (make-instance 'base-file-system :directory directory))))

(defmethod write-to-filesystem ((instruction compound-instruction)
                                file-system)
  (with-slots (instructions) instruction
    (dolist (instruction instructions)
      (declare (type file-system-instruction instruction))
      (write-to-filesystem instruction file-system))))

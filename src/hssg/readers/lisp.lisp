;;;; SPDX-License-Identifier AGPL-3.0-or-later

;;;; lisp.lisp  Lisp content reader implementation
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.

(defpackage #:hssg.reader.lisp
  (:documentation "Lisp reader implementation")
  (:use :cl))
(in-package #:hssg.reader.lisp)

;;; Idea: if creating a new package is too much effort we could create a
;;; reserved package that can be cleaned and recycled between reads (make sure
;;; to synchronize on the package if multiple reader can read concurrently).
(defmacro with-package ((&rest using) &body body)
  "Evaluate the BODY expressions within a separate package."
  ;; FIXME: the new package is not actually used.
  (let ((name (gensym))
        (package (gensym)))
    `(let* ((,name (gensym))
            (,package (make-package ,name :use (list ,@using))))
       (unwind-protect (let ((*PACKAGE* ,package))
                         ,@body)
         (delete-package ,package)))))

(defun read-lisp-metadata (pathname)
  "Read the contents of an HTML document from a Lisp file."
  (declare (type (or string pathname) pathname))
  (with-open-file (*STANDARD-INPUT* pathname)
    (with-package (:cl)
      (loop for exp = (read *STANDARD-INPUT* nil)
            with value
            if (null exp) return value
            else do (setf value (eval exp))))))

(hssg.reader:register-reader "lisp" #'read-lisp-metadata)

;;;; SPDX-License-Identifier AGPL-3.0-or-later

;;;; template.lisp  Definition of HTML template macros
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.
(in-package #:hssg.template)

(deftype template ()
  "An HTML template, either a function object which maps a data
  association-list to another association list, or a symbol whose function is a
  template function."
  '(or symbol function))

(defmacro template ((&rest bindings) &body entries)
  "Define an anonymous template.  A template is a function which maps an a-list
  onto another a-list.

  For each binding in BINDINGS bind the corresponding value from the input
  a-list.  These bindings can then be used inside the individual ENTRIES."
  (let ((data (gensym "METADATA")))
    `(lambda (,data)
       (let (,@(mapcar (lambda (binding)
                         `(,binding (cdr (assoc (intern ,(symbol-name binding) "KEYWORD")
                                                ,data :test #'eq))))
                       bindings))
         (append
           (list ,@(mapcar
                     #'(lambda (entry)
                         (destructuring-bind (field value) entry
                           `(cons (intern ,(symbol-name field) "KEYWORD")
                                  ,value)))
                     entries))
           ,data)))))

(defmacro deftemplate (name (&rest bindings) &body entries)
  "Define a named template, which is just an ordinary function.  Refer to
  TEMPLATE for details."
  (let* ((docstring (and (typep (car entries) 'string)
                         (car entries)))
         (entries (if docstring (cdr entries) entries)))
    `(progn
       (setf (fdefinition ',name)
             (template ,bindings ,@entries))
       (setf (documentation ',name 'function) ,docstring)
       ',name)))

(defmacro let-metadata ((&rest bindings) data-expr &body body)
  "Binds the metadata from the DATA-EXPRession to the BINDINGS for the
  evaluation of the BODY expressions.  Each bindings is a tuple of the form
  (KEYWORD SYMBOL &OPTIONAL DEFAULT).  The DEFAULT expression will only be
  evaluated if the KEYWORD is not present in the DATA.

  The result is the value of the last BODY form.

  Example:
    (let ((data '((:a . \"a\") (:b . \"b\"))))
      (let-metadata ((a :a)
                     (b :b)
                     (c :c \"c\")  ; Explicit default
                     (d :d))       ; Implicit default
          data 
        (list a b c d)))
    --
    (\"a\" \"b\" \"c\" NIL)"
  (let ((data (gensym "DATA")))
    (flet ((transform-binding (binding)
             (destructuring-bind (identifier kword &optional (default nil)) binding
               (let ((entry (gensym)))
                 `(,identifier (let ((,entry (assoc ,kword ,data)))
                                (if ,entry (cdr ,entry) ,default)))))))
      `(let ((,data ,data-expr))
         (let (,@(mapcar #'transform-binding bindings))
           ,@body)))))


;; Alternative: use symbol as template name, then use (SYMBOL-FUNCTION
;; TEMPLATE) to get the function.
(defmacro apply-template (template data)
  "Apply the data transformation of TEMPLATE to the DATA."
  (let ((template-var (gensym))
        (data-var     (gensym)))
    `(let ((,template-var ,template)
           (,data-var     ,data))
       (etypecase ,template-var
         (symbol (funcall (symbol-function ,template-var) ,data-var))
         (function (funcall ,template-var ,data-var))))))

(deftemplate identity-template ()
  "The neutral template, returns its input data unchanged. (I don't know why
  anyone would need this, but it is necessary in order to make the template
  type a monoid.)"
  ;; Intentionally left blank
  )

(defun chain-templates (&rest templates)
  "Returns a template which will apply all TEMPLATES in order from left to
  right one after another to the data. (This is the operation of the template
  monoid)"
  (lambda (data)
    (declare (type list data))
    (let ((result data))
      (declare (type list result))
      (dolist (tmp templates result)
        (declare (type template tmp))
        (setf result (apply-template tmp result))))))

(defun template-with-data (template initial-data)
  "Returns a new template which calls TEMPLATE with INITIAL-DATA and the data
  passed to the template."
  (declare (type template template)
           (type list initial-data))
  (lambda (data) (funcall template (concatenate 'list data initial-data))))

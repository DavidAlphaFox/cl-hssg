;;;; SPDX-License-Identifier AGPL-3.0-or-later

;;;; artifact.lisp  Public artifact protocol
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.
(in-package #:hssg.artifact)

(defgeneric derive-artifact (artifact)
  (:documentation "Derives the GIVEN artifact to produce a file system instruction."))

(defun write-artifact* (artifact file-system) 
  "Derives the given ARTIFACT and writes it to the FILE-SYSTEM."
  (declare (type file-system file-system))
  (let ((instruction (derive-artifact artifact)))
    (declare (type file-system-instruction instruction))
    (write-to-filesystem instruction file-system)))

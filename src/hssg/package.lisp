;;;; SPDX-License-Identifier: AGPL-3.0-or-later

;;;; package.lisp  Package definitions
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.

;;; ---------------------------------------------------------------------------
(defpackage #:hssg.filesystem
  (:documentation "File system abstraction.")
  (:use :cl)
  (:export #:file-system #:base-file-system #:overlay-file-system
           #:file-system-instruction
           #:write-string-contents #:copy-file #:copy-directory #:compound-instruction
           #:write-to-filesystem))


;;; ---------------------------------------------------------------------------
(defpackage #:hssg.reader
  (:documentation "Interface to the various content readers.")
  (:use :cl)
  (:export get-reader register-reader unregister-reader with-readers))

(defpackage #:hssg.artifact
  (:documentation "Helper package, defines the artifact protocl.")
  (:use #:cl)
  (:import-from #:hssg.filesystem
    #:file-system-instruction #:file-system #:write-to-filesystem)
  (:export #:write-artifact #:derive-artifact artifacts
           compound-artifact verbatim-artifact directory-artifact html-artifact xml-artifact))


;;; ---------------------------------------------------------------------------
(defpackage #:hssg.artifact._compound
  (:documentation "Implementatio of compound HSSG artifacts.")
  (:use :cl)
  (:export make-compound-artifact compound-artifact-push))

(defpackage #:hssg.artifact.directory
  (:documentation "Helper package, contains verbatim artifacts")
  (:use #:cl)
  (:export make-directory-artifact))

(defpackage #:hssg.artifact.html
  (:documentation "Implementaion of HTML artifacts.")
  (:use #:cl)
  (:export static-page read-html-lisp))

(defpackage #:hssg.artifact.xml
  (:documentation "Implementation of XML artifacts.")
  (:use #:cl)
  (:export))

(defpackage #:hssg.artifact.verbatim
  (:documentation "Helper package, contains verbatim artifacts")
  (:use #:cl)
  (:export make-verbatim-artifact))

(defpackage #:hssg.template
  (:documentation "HTML template macros; templates transform SHTML aslists.")
  (:use #:cl)
  (:export deftemplate template let-metadata apply-template identity-template
           chain-templates template-with-data))

(defpackage #:hssg
  (:documentation "The hackable static site generator")
  (:use #:cl)
  (:import-from #:hssg.filesystem
     #:file-system #:base-file-system #:overlay-file-system
     #:file-system-instruction
     #:write-string-contents #:copy-file #:copy-directory #:compound-instruction
     #:write-to-filesystem)
  (:import-from #:hssg.artifact write-artifact #:derive-artifact artifacts html-artifact xml-artifact)
  (:import-from #:hssg.artifact._compound make-compound-artifact compound-artifact-push)
  (:import-from #:hssg.artifact.directory make-directory-artifact)
  (:import-from #:hssg.artifact.verbatim make-verbatim-artifact)
  (:import-from #:hssg.artifact.html static-page read-html-lisp)
  (:import-from #:hssg.template deftemplate template let-metadata apply-template identity-template
                chain-templates template-with-data)
  (:export
    *site-url* *site-language*
    ;; File system
    #:file-system #:base-file-system #:overlay-file-system
    #:file-system-instruction
    #:write-string-contents #:copy-file #:copy-directory #:compound-instruction
    #:write-to-filesystem
    ;; Artifact protocol
    write-artifact #:derive-artifact
    ;; Compound artifacts
    make-compound-artifact compound-artifact-push
    
    html-artifact xml-artifact static-page read-html-lisp
    make-verbatim-artifact make-directory-artifact
    deftemplate template let-metadata apply-template identity-template chain-templates template-with-data))

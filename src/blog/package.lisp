;;;; SPDX-License-Identifier: AGPL-3.0-or-later

;;;; package.lisp  Package interfaces to the blog plugin
;;;;
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.
(defpackage #:hssg.blog.util
  (:documentation "Various helpers")
  (:use :cl)
  (:export range date->year break-list
           date-from-numbers date->month date->day date->string date->url date->pathname
           intersperse directory-contents))

(defpackage #:hssg.blog.i18n
  (:documentation "Internationalisation support")
  (:use :cl)
  (:export *month-names* *default-category* *localisations* localise))

(defpackage #:hssg.blog.period
  (:use :cl)
  (:export
    year-period year months
    month-period month days
    day-period day posts
    period-posts period-length))

(defpackage #:hssg.blog.artifacts
  (:documentation "Helper package, implements all blog artifact classes")
  (:use :cl :hssg.blog.util)
  (:import-from #:hssg write-artifact)
  (:export
    blog-artifact title description posts categories tags authors periods top url template initial static
    post-artifact blog slug content category author published modified status previous next metadata
    categories-artifact items
    category-artifact name category-name
    tags-artifact
    tag-artifact tag-name
    index-page-artifact number total
    archive-page-artifact
    rss-feed-artifact))

(defpackage #:hssg.blog.artifact.util
  (:documentation "Helper package, various reusable utility functions.")
  (:use :cl)
  (:export #:collect-index-pages))

(defpackage #:hssg.blog.artifact.archive
  (:use :cl)
  (:import-from #:hssg.blog.artifacts archive-page-artifact))

(defpackage #:hssg.blog.artifacts.blog
  (:use :cl)
  (:import-from #:local-time encode-timestamp)
  (:import-from #:hssg write-artifact)
  (:import-from #:hssg.blog.util date-from-numbers date->year date->month date->day)
  (:import-from #:hssg.blog.artifacts
                blog-artifact posts categories tags authors periods title static
                categories-artifact items
                category-artifact
                tags-artifact tag-artifact
                post-artifact blog slug content category tags author published modified))

(defpackage #:hssg.blog.artifact.category
  (:use :cl)
  (:import-from #:hssg.blog.artifacts categories-artifact category-artifact)
  (:export add-post))

(defpackage #:hssg.blog.artifact.index-page
  (:use :cl)
  (:import-from #:hssg.blog.artifacts index-page-artifact))

(defpackage #:hssg.blog.artifact.post
  (:use :cl)
  (:import-from #:hssg.blog.artifacts post-artifact))

(defpackage #:hssg.blog.artifact.rss-feed
  (:use :cl)
  (:import-from #:hssg.blog.artifacts rss-feed-artifact))

(defpackage #:hssg.blog.artifact.tag
  (:use :cl)
  (:import-from #:hssg.blog.artifacts tags-artifact tag-artifact)
  (:import-from #:hssg.blog.artifact.util #:collect-index-pages)
  (:export add-post))

(defpackage #:hssg.blog.template
  (:documentation "Internal helper package, contains the SHTML templates.")
  (:use :cl)
  (:export blog-page post article-index index categories tags archive))

(defpackage #:hssg.blog.template.archive
  (:documentation "Internal helper package.")
  (:use :cl)
  (:export archive-year->sxml archive-month->sxml archive-day->sxml archive-post->sxml))

(defpackage #:hssg.blog.template.blog
  (:documentation "Internal helper package.")
  (:use :cl)
  (:export breadcrumb->sxml blog-category->sxml blog-tag->sxml period->sxml))

(defpackage #:hssg.blog.template.category&tag
  (:documentation "Internal helper package.")
  (:use :cl)
  (:export category->sxml tag->sxml))

(defpackage #:hssg.blog.template.index
  (:documentation "Internal helper package.")
  (:use :cl)
  (:export post->preview paginator))

(defpackage #:hssg.blog.template.post
  (:documentation "Internal helper package.")
  (:use :cl)
  (:export post-tag->sxml pager->sxml))

(defpackage #:hssg.blog.facade
  (:use :cl)
  (:export make-blog))

(defpackage #:hssg.blog
  (:documentation "CommonMark parser extension for HSSG; parses CommonMark
  files to SXML trees")
  (:use #:cl)
  (:import-from #:hssg.blog.i18n *month-names* *default-category* *localisations*)
  (:import-from #:hssg.blog.facade make-blog)
  (:export make-blog
           *month-names* *default-category* *localisations*
           *posts-per-page*
           *blog-post-readers*))

;;;; SPDX-License-Identifier: AGPL-3.0-or-later

;;;; classes.lisp  Class definition for the various time periods a post can be in
;;;;
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.
(in-package #:hssg.blog.period)

(defclass year-period ()
  ((year :initarg :year :type integer
         :documentation "The year in BC/AD.")
   (months :initform '() :type list
           :documentation "Months in the year which have blog posts."))
  (:documentation "A year's worth of blog posts"))

(defclass month-period ()
  ((month :initarg :month :type (integer 1 12)
          :documentation "The month, starting at 1 (January).")
   (days :initform '() :initarg :days :type list
         :documentation "Days in the month which have blog posts."))
  (:documentation "A months's worth of blog posts"))

(defclass day-period ()
  ((day :initarg :day :type (integer 1 31)
        :documentation "The day of the month.")
   (posts :initform '() :initarg :posts :type list
          :documentation "Posts of the day."))
  (:documentation "A day's worth of blog posts"))

;;;; SPDX-License-Identifier: AGPL-3.0-or-later

;;;; protocol.lisp  Implementation of the period class protocol
;;;;
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.
(in-package #:hssg.blog.period)

(defgeneric period-posts (period)
  (:documentation "Returns all the posts cotained inside the PERIOD.")
  (:method ((period year-period))
    (apply #'concatenate 'list (mapcar #'period-posts (slot-value period 'months))))
  (:method ((period month-period))
    (apply #'concatenate 'list (mapcar #'period-posts (slot-value period 'days))))
  (:method ((period day-period))
    (slot-value period 'posts)))

(defgeneric period-length (period)
  (:documentation "Returns the total number of posts for a given period")
  (:method ((period year-period))
    (reduce #'+ (mapcar #'period-length (slot-value period 'months))))
  (:method ((period month-period))
    (reduce #'+ (mapcar #'period-length (slot-value period 'days))))
  (:method ((period day-period))
    (length (slot-value period 'posts))))

;;;; SPDX-License-Identifier: AGPL-3.0-or-later

;;;; index.lisp  Helper functions for the index template
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.
(in-package #:hssg.blog.template.index)

(defun post->preview (post)
  "Converts one post artifact to an SHTML tree of its preview."
  (declare (type hssg.blog.artifacts:post-artifact post))
  (with-slots ((title hssg.blog.artifacts:title)
               (content hssg.blog.artifacts:content)
               (published hssg.blog.artifacts:published)
               (modified hssg.blog.artifacts:modified)
               (blog hssg.blog.artifacts:blog)
               (slug hssg.blog.artifacts:slug)) post
    (with-slots ((url hssg.blog.artifacts:url)) blog
      (let ((url (format nil "/~{~A/~}~A/~A/" url (hssg.blog.util:date->url published) slug)))
        `(:li
          (:article
           (:h1 ((:a :href ,url) ,title))
           ((:header :class "blog-post-header")
            (:p
             ((:time :datetime ,(hssg.blog.util:date->string published))
              ,(format nil "~A: ~A" (hssg.blog.i18n:localise :published)
                                    (hssg.blog.util:date->string published))))
            ,(if modified
                 `(:p
                   ((:time :datetime ,(hssg.blog.util:date->string modified))
                    ,(format nil "~A: ~A" (hssg.blog.i18n:localise :modified)
                                          (hssg.blog.util:date->string modified))))))
           ,(car content)
           ,(if (cdr content)
                `(:p
                  ((:a :href ,url)
                    ,(hssg.blog.i18n:localise :continue))))))))))

(defun paginator (current-page total-pages)
  (declare (type (integer 1) current-page total-pages))
  (flet ((page-index->sxml (index &optional (label nil))
           `((:li :class ,(if (= index current-page) "active" ""))
              ,(if (= index current-page)
                 ;; Only display the number, but don't make it a hyperlink
                 `(:a 
                    ,(format nil "~A" index))
                 ;; Make it a hyperlink
                 `((:a :href ,(cond
                               ((= 1 current-page) (format nil "./~A/" index))
                               ((= 1        index) "..")
                               (t (format nil "../~A/" index))))
                    ,(if label label (format nil "~A" index)))))))
    (cond
      ;; If there are fewer than eight pages display them all
      ((< total-pages 8)
       (mapcar #'page-index->sxml (hssg.blog.util:range 1 total-pages)))
      ;; There are more than eight pages, but we are still in the first batch
      ((< current-page 5)
       `(;; Display a previous item if We are not on the first page
         ,(if (> current-page 1)
            (funcall #'page-index->sxml (1- current-page) `((:span :aria-hidden "true") "‹"))
            '())
         ;; The first seven pages
         ,@(mapcar #'page-index->sxml (hssg.blog.util:range 1 7))
         ;; The next page
         ,(funcall #'page-index->sxml (1+ current-page) `((:span :aria-hidden "true") "›"))
         ;; The last page
         ,(funcall #'page-index->sxml total-pages `((:span :aria-hidden "true") "»"))))
		  ;; Similar to the previous case, but in the last batch
      ((> current-page (- total-pages 4))
       `(;; The first page
         ,(funcall #'page-index->sxml 1 `((:span :aria-hidden "true") "«"))
         ;; The previous page
         ,(funcall #'page-index->sxml (1- current-page) `((:span :aria-hidden "true") "‹"))
         ;; The last seven pages
         ,@(mapcar #'page-index->sxml (hssg.blog.util:range (- total-pages 6) total-pages))
         ;; Display a next page if we are not on the last page
         ,(if (< current-page total-pages)
            (funcall #'page-index->sxml (1+ current-page) `((:span :aria-hidden "true") "›"))
            '())))
      ;; Somewhere in the middle, display everything
      (t
       `(;; The first page
         ,(funcall #'page-index->sxml 1 `((:span :aria-hidden "true") "«"))
         ;; The previous page
         ,(funcall #'page-index->sxml (1- current-page) `((:span :aria-hidden "true") "‹"))
         ;; ...
         ,@(mapcar #'page-index->sxml (hssg.blog.util:range (- current-page 3) (+ current-page 3)))
         ;; The next page
         ,(funcall #'page-index->sxml (1+ current-page) `((:span :aria-hidden "true") "›"))
         ;; The last page
         ,(funcall #'page-index->sxml total-pages `((:span :aria-hidden "true") "»")))))))

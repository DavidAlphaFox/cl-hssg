;;;; SPDX-License-Identifier: AGPL-3.0-or-later

;;;; archive.lisp  Helper functions for the archive template
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.
(in-package #:hssg.blog.template.archive)

(defun archive-year->sxml (period)
  (declare (type hssg.blog.period:year-period period))
  (with-slots ((year hssg.blog.period:year)
               (months hssg.blog.period:months))
      period
    `(:li ((:a :href ,(format nil "../~D/" year))
            ,(format nil "~D" year))
       (:ul ,@(mapcar (lambda (month)
                        (archive-month->sxml month year))
                      months)))))

(defun archive-month->sxml (period year)
  (declare (type hssg.blog.period:month-period period))
  (with-slots ((month hssg.blog.period:month)
               (days hssg.blog.period:days))
      period
    (let ((name (cdr (assoc month hssg.blog.i18n:*month-names*))))
      `(:li ((:a :href ,(format nil "../~D/~2,'0D/" year month))
              ,(format nil "~A" name))
        (:ul ,@(apply #'concatenate 'list (mapcar (lambda (day)
                                                    (archive-day->sxml day year month))
                                                  days)))))))

(defun archive-day->sxml (period year month)
  (declare (type hssg.blog.period:day-period period))
  (with-slots ((day hssg.blog.period:day)
               (posts hssg.blog.period:posts))
      period
    (mapcar (lambda (post)
              (archive-post->sxml post year month day))
            posts)))

(defun archive-post->sxml (post year month day)
  (declare (type hssg.blog.artifacts:post-artifact post)
           (type integer year month day))
  (with-slots ((title hssg.blog.artifacts:title)
               (slug  hssg.blog.artifacts:slug)) post
    `(:li ((:a :href ,(format nil "../~D/~2,'0D/~2,'0D/~A/" year month day slug))
            ,title))))

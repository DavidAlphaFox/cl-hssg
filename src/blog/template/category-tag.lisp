;;;; SPDX-License-Identifier: AGPL-3.0-or-later

;;;; category-tag.lisp  Helper functions for the category- and tag templates
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.
(in-package #:hssg.blog.template.category&tag)

(defun category->sxml (category)
  (declare (type hssg.blog.artifacts:category-artifact category))
  (with-slots ((name  hssg.blog.artifacts:name)
               (posts hssg.blog.artifacts:posts))
      category
    `(:li
       ((:a :href ,(format nil "~A/" name)) ,name)
       ,(format nil " (~D)" (length posts)))))

(defun tag->sxml (tag)
  (declare (type hssg.blog.artifacts:tag-artifact tag))
  (with-slots ((name  hssg.blog.artifacts:name)
               (posts hssg.blog.artifacts:posts))
      tag
    `(:li
       ((:a :href ,(format nil "~A/" name)) ,name)
       ,(format nil " (~D)" (length posts)))))


;;;; SPDX-License-Identifier: AGPL-3.0-or-later

;;;; post.lisp  Helper functions for the post template
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.
(in-package #:hssg.blog.template.post)

(defun post-tag->sxml (tag)
  "Convert one TAG name into a hyperlink to the index of the tag."
  (declare (type string tag))
  (let ((tags-url (hssg.blog.i18n:localise :url-tags)))
    `((:a :href ,(format nil "../../../../~A/~A/" tags-url tag))
       ,tag)))

(defun pager->sxml (prev next)
  "Build the SXML tree for the pager of a blog post."
  (declare (type (or null hssg.blog.artifacts:post-artifact) prev next) )
  (flet ((item->sxml (item rel symbol next?)
           (declare (type hssg.blog.artifacts:post-artifact item)
                    (type string rel symbol)
                    (type boolean next?))
           (with-slots ((title     hssg.blog.artifacts:title)
                        (slug      hssg.blog.artifacts:slug)
                        (published hssg.blog.artifacts:published)
                        (blog      hssg.blog.artifacts:blog))
               item
             `((:a :href ,(format nil "/~{~A/~}~A/~A/" (slot-value blog 'hssg.blog.artifacts:url)
                                                       (hssg.blog.util:date->url published)
                                                       slug)
                   :rel ,rel
                   :style ,(format nil "float: ~A" (if next? "right" "left")))
                ,@(if next?
                    `(,title " " ((:span :aria-hidden "true") ,symbol))
                    `(((:span :aria-hidden "true") ,symbol) " " ,title))))))
    `((:nav :class "blog-pager")
       ,(if prev
          (item->sxml prev "previous" "←" nil)
          '((:a :hidden "hidden") ""))
       ,(if next
          (item->sxml next "next" "→" t)
          '((:a :style "display: none;") "")))))

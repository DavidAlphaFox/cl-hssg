;;;; SPDX-License-Identifier: AGPL-3.0-or-later

;;;; blog.lisp  Helper functions for the blog page template
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.
(in-package #:hssg.blog.template.blog)

(defun breadcrumb->sxml (item)
  "Convert a breadcrumb entry from the items to an SXML tree"
  (declare (type list item))
  (hssg:let-metadata ((title :title)
                      (url :url))
      item
    `((:li :class ,(if url "" "active"))
       ,(if url
          `((:a :href ,url) ,title)
          title))))

(defun blog-category->sxml (category blog-url)
  "Convert an item from the categories alist into an SXML tree."
  (declare (type hssg.blog.artifacts:category-artifact category)
           (type list blog-url))
  (with-slots ((name  hssg.blog.artifacts:name)
               (posts hssg.blog.artifacts:posts))
      category
    `(:li
        ((:a :href ,(format nil "/~{~A/~}~A/~A/" blog-url (hssg.blog.i18n:localise :url-categories) name))
           ,(format nil "~A (~D)" name (length posts))))))

(defun blog-tag->sxml (tag blog-url)
  "Convert an item from the tags list into an SXML tree."
  (declare (type hssg.blog.artifacts:tag-artifact tag)
           (type list blog-url))
  (with-slots ((name  hssg.blog.artifacts:name)
               (posts hssg.blog.artifacts:posts))
      tag
    `(:li
      ((:a :href ,(format nil "/~{~A/~}~A/~A/" blog-url (hssg.blog.i18n:localise :url-tags) name))
       ,(format nil "~A (~D)" name (length posts))))))

(defun period->sxml (period blog-url)
  (declare (type hssg.blog.period:year-period period)
           (type list blog-url))
  (let ((year (slot-value period 'hssg.blog.period:year)))
    `(:li
       ((:a :href ,(format nil "/~{~A/~}~D/" blog-url year))
         ,(format nil "~D (~D)" year (hssg.blog.period:period-length period))))))

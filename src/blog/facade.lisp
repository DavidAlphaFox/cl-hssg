;;;; SPDX-License-Identifier: AGPL-3.0-or-later

;;;; facade.lisp  Implementation of the public interface to the blog plugin
;;;;
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.
(in-package #:hssg.blog.facade)

(defun make-blog (input &key (title "Blog") (url (list)) (description "") (template #'hssg:identity-template) (top ""))
  "Constructor function which produces a blog artifact. Parameters:
    - INPUT:        Pathname to the directory from which to read the blog
    - TITLE:        Title of the blog
    - URL:          URL path components to the blog, relative to the site root
    - TOP:          Identifier to use as the top of the breadcrumbs
    - DESCRIPTION:  Human-readable description, will be used for RSS feed
    - TEMPLATE:     Template to use for HTML pages"
  (declare (type pathname input)
           (type string title description top)
           (type list url)
           (type hssg:template template))
  (let ((result (make-instance 'hssg.blog.artifacts:blog-artifact
                  :title title :description description :url url :template template :top top))
        last-post)
    (setf (slot-value (slot-value result 'hssg.blog.artifacts:categories) 'hssg.blog.artifacts:blog) result)
    (setf (slot-value (slot-value result 'hssg.blog.artifacts:tags) 'hssg.blog.artifacts:blog) result)
    (dolist (year (hssg.blog.util:directory-contents input))
      (dolist (month (hssg.blog.util:directory-contents year))
        (dolist (day (hssg.blog.util:directory-contents month))
          (dolist (entry (hssg.blog.util:directory-contents day :test #'file-pathname-p))
            (let ((post (read-post entry result)))
              (when last-post
                (setf (slot-value post 'hssg.blog.artifacts:previous) last-post)
                (setf (slot-value last-post 'hssg.blog.artifacts:next) post))
              (add-blog-post result post)
              (setf last-post post)))
          (dolist (directory (hssg.blog.util:directory-contents day :test #'fad:directory-pathname-p))
            (add-static
              result
              ;; The path needs to be relative, so we perform some PATHNAME trickery
              (enough-namestring directory (fad:pathname-parent-directory year))
              input)))))
    result))

(defun file-pathname-p (pathname)
  "Whether PATHNAME names a file."
  (not (fad:directory-pathname-p pathname)))

(defun read-post (file-path blog)
  "Reads one single blog post from file FILE-PATH, where the path of FILE-PATH
  determines the date of the post and file name the slug of the post."
  (declare (type pathname file-path)
           (type hssg.blog.artifacts:blog-artifact blog))
  (let ((date-components (mapcar #'parse-integer
                                 (last (pathname-directory file-path) 3)))
        (post (funcall (gethash (pathname-type file-path) hssg.blog:*BLOG-POST-READERS*)
                       file-path)))
    (hssg:let-metadata
        ((category :category hssg.blog.i18n:*default-category*)
         (slug :slug (pathname-name file-path))
         (title :title)
         (author :author)
         (tags :tags)
         (published :published (apply #'hssg.blog.util:date-from-numbers date-components))
         (modified :modified)
         (content :content))
        post
      (make-instance 'hssg.blog.artifacts:post-artifact :blog blog :slug slug :title title :content content
                     :category category :tags tags :author author
                     :published published
                     :modified modified
                     :metadata post))))

(defun add-blog-post (blog post)
  "Add a single blog post to the entire blog. The blog's author, category and
  tags will also be registered."
  (with-slots ((posts hssg.blog.artifacts:posts)
               (tags hssg.blog.artifacts:tags)
               (authors hssg.blog.artifacts:authors)
               (categories hssg.blog.artifacts:categories))
      blog
    (push post posts)
    (with-slots ((category-name hssg.blog.artifacts:category)
                 (post-tags hssg.blog.artifacts:tags)
                 (author hssg.blog.artifacts:author))
        post
      (when category-name
        (hssg.blog.artifact.category:add-post categories category-name post))
      (when post-tags
        (dolist (tag post-tags)
          (hssg.blog.artifact.tag:add-post tags tag post)))
      (when author
        (push post (gethash author authors))))
    ;; This assumes that incoming posts are ordered from oldest to newest!
    (with-slots ((periods hssg.blog.artifacts:periods)) blog
      (let ((year (local-time:timestamp-year (slot-value post 'hssg.blog.artifacts:published)))
            (top-period (car periods)))
        (when (or (not top-period) (not (= year (slot-value top-period 'hssg.blog.period:year))))
          (push (make-instance 'hssg.blog.period:year-period :year year) periods)))
      (push-post (car periods) post))))

(defun add-static (blog pathname input-dir)
  (with-slots ((static hssg.blog.artifacts:static))
      blog
    (let ((artifact (hssg:make-directory-artifact input-dir pathname)))
      (hssg:compound-artifact-push static artifact))))

(defmethod push-post ((period hssg.blog.period:day-period) post)
  (with-slots ((posts hssg.blog.period:posts)) period
    (push post posts)))

(defmethod push-post ((period hssg.blog.period:month-period) post)
  (with-slots ((days hssg.blog.period:days)) period
    (let ((day (local-time:timestamp-day (slot-value post 'hssg.blog.artifacts:published))))
      (let ((topmost (car days)))
        (if (or (null topmost) (not (= day (slot-value topmost 'hssg.blog.period:day))))
            (let ((day-period (make-instance 'hssg.blog.period:day-period :day day)))
              (push day-period days)
              (push-post day-period post))
            (push-post topmost post))))))

(defmethod push-post ((period hssg.blog.period:year-period) post)
  (with-slots ((months hssg.blog.period:months)) period
    (let ((month (local-time:timestamp-month (slot-value post 'hssg.blog.artifacts:published))))
      (let ((topmost (car months)))
        (if (or (null topmost) (not (= month (slot-value topmost 'hssg.blog.period:month))))
            (let ((month-period (make-instance 'hssg.blog.period:month-period :month month)))
              (push month-period months)
              (push-post month-period post))
            (push-post topmost post))))))

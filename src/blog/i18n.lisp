;;;; SPDX-License-Identifier: AGPL-3.0-or-later

;;;; i18n.lisp  Internationalization interface of the blog plugin
;;;;
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.
(in-package #:hssg.blog.i18n)

;;; TODO: Does this really need its own package? Perhaps it would make more
;;; sense to directly make it part of the global main package instead.

(defparameter *month-names*
  '((1 . "January")
    (2 . "February")
    (3 . "March")
    (4 . "April")
    (5 . "May")
    (6 . "June")
    (7 . "July")
    (8 . "August")
    (9 . "September")
    (10 . "October")
    (11 . "November")
    (12 . "December"))
  "Association list which maps the number of a month to its name; starting at
  January being 1.")

(defparameter *default-category* "misc"
  "Default category name to use for blog posts.")

(defparameter *localisations*
  '(("en" . ((:archive        . "archive")
             (:categories     . "categories")
             (:tags           . "tags")
             (:published      . "published")
             (:modified       . "modified")
             (:continue       . "Continue reading…")
             (:subscribe      . "subscribe")
             (:permalink      . "permalink to")
             (:draft          . "draft")
             (:draft-msg      . ("This article is in draft status, set the "
                                 (:code "status")
                                 " metadatum to "
                                 (:code "published")
                                 " when ready."))
             (:url-archive    . "archive")
             (:url-categories . "categories")
             (:url-tags       . "tags")
             (:url-authors    . "authors"))))
  "Association list of blog text navigation. Blog text is any text which is
  inserted by default into the various blog pages, usually labels for
  navigation or the header portion of a blog post.

  The key is the language code to use, the value is another association list.
  Inside that other list the key is a unique identifier and the value is the
  translation.

  There are separate identifiers for the display and for the URL. This is
  intended for languages where it is desireable to differentiate between the
  two, e.g. because the language uses a non-Latin alphabet or has non ASCII
  characters.")

(defun localise (identifier)
  "Returns the localised value for IDENTIFIER in the current language of the
  site."
  (let ((localisation (assoc hssg:*site-language* *localisations* :test 'equal)))
    (unless localisation
      (return-from localise nil))
    (let ((entry (assoc identifier (cdr localisation))))
      (and entry (cdr entry)))))

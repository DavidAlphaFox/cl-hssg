;;;; SPDX-License-Identifier: AGPL-3.0-or-later

;;;; lisp.lisp  Implementation of the lisp reader
;;;;
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.

(defpackage #:hssg.blog.reader.lisp
  (:documentation "Helper package which exposes the Lisp blog post reader.")
  (:use :cl)
  (:export read-lisp))
(in-package #:hssg.blog.reader.lisp)

;;; TODO: should use the reader from the HSSG core
(defmacro with-package ((&rest using) &body body)
  "Evaluate the BODY expressions within a separate package."
  ;; FIXME: the new package is not actually used.
  (let ((name (gensym))
        (package (gensym)))
    `(let* ((,name (gensym))
            (,package (make-package ,name :use (list ,@using))))
       (unwind-protect (let ((*PACKAGE* ,package))
                         ,@body)
         (delete-package ,package)))))


(defun read-lisp (pathname)
  "Reads the contents of a Common Lisp file into a blog post"
  (declare (type pathname pathname))
  (let ((data (with-open-file (*STANDARD-INPUT* pathname)
                (with-package (:cl)
                  (loop for exp = (read *STANDARD-INPUT* nil)
                        with value
                        if (null exp) return value
                        else do (setf value (eval exp)))))))
    data))

(setf (gethash "lisp" hssg.blog:*BLOG-POST-READERS*) #'read-lisp)

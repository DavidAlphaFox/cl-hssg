;;;; SPDX-License-Identifier AGPL-3.0-or-later

;;;; config.lisp  Mutable global variables for configuring the entire generator
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.
(in-package #:hssg.blog.artifact.category)

(defun fetch-category (categories name)
  "Retrieve the category object with a given NAME from a BLOG artifact; if
  there is no category object it is created and registered first."
  (declare (type hssg.blog.artifacts:categories-artifact categories)
           (type string name))
  (with-slots ((items hssg.blog.artifacts:items)
               (blog  hssg.blog.artifacts:blog))
      categories
    (or (find name items :test #'string= :key #'hssg.blog.artifacts:category-name)
        (let ((result (make-instance 'category-artifact :name name :blog blog)))
          (push result items)
          result))))

(defun add-post (categories name post)
  "Adds a single POST to the CATEGORIES collection under the key NAME."
  (with-slots ((posts hssg.blog.artifacts:posts))
      (fetch-category categories name)
    (push post posts)))

(defun categories->html (items blog) 
  "Reduce them ITEMS of a CATEGORIES-ARTIFACT instance to an HTML page
  artifact."
  (make-instance 'hssg:html-artifact
    :data `((:categories . ,items)
            (:blog       . ,blog)
            ,@(slot-value blog 'hssg.blog.artifacts:initial))
    :template (hssg:chain-templates
                'hssg.blog.template:categories
                'hssg.blog.template:blog-page
                (slot-value blog 'hssg.blog.artifacts:template))
    :output (fad:merge-pathnames-as-file
              (fad:pathname-as-directory
                (hssg.blog.i18n:localise :url-categories))
              (fad:pathname-as-file "index.html"))))


(defmethod hssg:derive-artifact ((category category-artifact))
  "Compound instruction, one instruction per index page."
  (with-slots ((blog  hssg.blog.artifacts:blog)
               (name  hssg.blog.artifacts:name)
               (posts hssg.blog.artifacts:posts))
      category
    (make-instance 'hssg:compound-instruction
      :instructions (mapcar
                      #'hssg:derive-artifact
                      (hssg.blog.artifact.util:collect-index-pages
                          posts blog (list (hssg.blog.i18n:localise :url-categories) name))))))

(defmethod hssg:derive-artifact ((artifact categories-artifact))
  "Compound instruction: one instructions for the whole categories page and one
  instruction per individual category (which itself is a compound instruction)."
  (with-slots ((items hssg.blog.artifacts:items)
               (blog  hssg.blog.artifacts:blog))
      artifact
    (let ((categories-page  (hssg:derive-artifact (categories->html items blog)))
          (category-indeces (mapcar #'hssg:derive-artifact items)))
      (make-instance 'hssg:compound-instruction
        :instructions (cons categories-page category-indeces)))))

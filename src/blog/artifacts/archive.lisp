;;;; SPDX-License-Identifier: AGPL-3.0-or-later

;;;; archive.lisp  Implementation of blog post archive artifact
;;;;
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.
(in-package #:hssg.blog.artifact.archive)

(defun archive->html (archive) 
  "Transforms a given archive page artifact to an HTML page artifact."
  (with-slots ((periods hssg.blog.artifacts:periods)
               (blog    hssg.blog.artifacts:blog))
      archive
    (with-slots ((url      hssg.blog.artifacts:url)
                 ; (output   hssg.blog.artifacts:output)
                 (template hssg.blog.artifacts:template)
                 (initial  hssg.blog.artifacts:initial))
        blog
      (let ((data `((:periods . ,periods) (:blog . ,blog) ,@initial))
            (template (hssg:chain-templates
                        'hssg.blog.template:archive 'hssg.blog.template:blog-page template))
            (output (apply #'fad:merge-pathnames-as-file
                           `(;,output
                             ,(fad:pathname-as-directory "archive")
                             ,(fad:pathname-as-file "index.html")))))
        (make-instance 'hssg:html-artifact
          :data data
          :template template
          :output output)))))

(defmethod hssg:derive-artifact ((artifact archive-page-artifact))
  (with-slots ((periods hssg.blog.artifacts:periods)
               (blog    hssg.blog.artifacts:blog))
      artifact
    (with-slots ((url      hssg.blog.artifacts:url)
                 ; (output   hssg.blog.artifacts:output)
                 (template hssg.blog.artifacts:template)
                 (initial  hssg.blog.artifacts:initial))
        blog
      (let ((html-page (make-instance 'hssg:html-artifact
                                      :data `((:periods . ,periods)
                                              (:blog . ,blog)
                                              ,@initial)
                                      :template (hssg:chain-templates
                                                  'hssg.blog.template:archive
                                                  'hssg.blog.template:blog-page
                                                  template)
                                      :output (apply #'fad:merge-pathnames-as-file
                                                  `(,(fad:pathname-as-directory "archive")
                                                    ,(fad:pathname-as-file "index.html"))))))
        (hssg:derive-artifact html-page)))))

;;;; SPDX-License-Identifier AGPL-3.0-or-later

;;;; rss-feed.lisp  Implementation of the "rss-feed" file artifact
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.
(in-package #:hssg.blog.artifact.rss-feed)

(defun post->rss-post (post url)
  "Converts one post artifact to S-XML suitable for a post inside the RSS feed"
  (declare (type hssg.blog.artifacts:post-artifact post)
           (type list url))
  (with-slots ((title     hssg.blog.artifacts:title)
               (slug      hssg.blog.artifacts:slug)
               (published hssg.blog.artifacts:published)
               (category  hssg.blog.artifacts:category)
               (content   hssg.blog.artifacts:content)
               (blog      hssg.blog.artifacts:blog))
      post
    (with-slots ((top hssg.blog.artifacts:top))
        blog
      (let ((link (format nil "~{~A/~}~A/~A/" url (hssg.blog.util:date->url published) slug)))
        `(:item
           (:title ,title)
           (:link ,link)
           (:description ,(detag (car content)))
           ((:guid "isPermaLink" "true") ,link)
           ("pubDate" ,(local-time:format-rfc1123-timestring nil published))
           ,(when category
              `(:category ,category)))))))

(defun detag (data)
  "Strips DATA of all tags and concatenates the resulting strings. This leaves
  only the plain-text content without any markup, which is necessary for RSS
  feeds."
  (declare (type (or string list) data)
           (optimize (speed 3) (safety 0)))
  (the string
    (etypecase data
      (string data)
      (list
        (apply #'concatenate 'string (mapcar #'detag (cdr data)))))))

(defun rss->xml (rss-feed) 
  "Transforms an RSS feed to an XML artifact."
  (with-slots ((posts hssg.blog.artifacts:posts)
               (blog  hssg.blog.artifacts:blog))
      rss-feed
    (with-slots ((title       hssg.blog.artifacts:title)
                 (description hssg.blog.artifacts:description)
                 ; (output      hssg.blog.artifacts:output)
                 (url         hssg.blog.artifacts:url))
        blog
      (make-instance 'hssg:xml-artifact
        :output (fad:pathname-as-file "rss.xml")
        :data `((:rss :version "2.0")
                (:channel
                   (:title ,title)
                   (:link
                      ,(format nil "~A/~{~A/~}" hssg:*site-url* url))
                   (:description ,description)
                   (:generator "HSSG")
                   (:language ,hssg:*site-language*)
                   (:docs "https://www.rssboard.org/rss-2-0")
                   ("lastBuildDate"
                      ,(local-time:format-rfc1123-timestring nil (local-time:now)))
                   ,@(mapcar (lambda (post)
                               (post->rss-post post (cons hssg:*site-url* url)))
                             posts)))))))

(defmethod hssg:derive-artifact ((rss-feed rss-feed-artifact))
  (let ((xml-artifact (rss->xml rss-feed)))
    (hssg:derive-artifact xml-artifact)))

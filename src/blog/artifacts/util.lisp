;;;; SPDX-License-Identifier AGPL-3.0-or-later

;;;; util.lisp  Various utilities shared by all artifact implementations
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.
(in-package #:hssg.blog.artifact.util)

(defun collect-index-pages (posts blog url)
  "For a given list of POSTS break them into groups of size *POSTS-PER-PAGE*
  and create an index page artifact for each one. Returns a list of index page
  artifacts."
  (declare (type list posts url)
           (type hssg.blog.artifacts:blog-artifact blog))
  (do ((result '())
       (i 1 (incf i))
       previous)
      ((null posts) (progn
                      (dolist (index result)
                        (setf (slot-value index 'hssg.blog.artifacts:total) (1- i)))
                      (nreverse result)))
    (multiple-value-bind (current remainder) (hssg.blog.util:break-list posts hssg.blog:*posts-per-page*)
      (let ((artifact (make-instance 'hssg.blog.artifacts:index-page-artifact
                                     :url (if (= i 1)
                                              url
                                              (append url (list (format nil "~D" i))))
                                     :posts current :number i
                                     :prev previous :next nil :blog blog)))
        (when previous
          (setf (slot-value previous 'hssg.blog.artifacts:next) artifact))
        (setf previous artifact)
        (push artifact result))
      (setf posts remainder))))

;;;; SPDX-License-Identifier AGPL-3.0-or-later

;;;; blog.lisp  Implementation of the entire "blog" artifact
;;;; Copyright (C) 2022  Alejandro "HiPhish" Sanchez
;;;;
;;;; This file is part of CL-HSSG.
;;;;
;;;; CL-HSSG is free software: you can redistribute it and/or modify it under
;;;; the terms of the GNU Affero General Public License as published by the
;;;; Free Software Foundation, either version 3 of the License, or (at your
;;;; option) any later version.
;;;;
;;;; CL-HSSG is distributed in the hope that it will be useful, but WITHOUT ANY
;;;; WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
;;;; FOR A PARTICULAR PURPOSE.  See the GNU Affero General Public License for
;;;; more details.
;;;;
;;;; You should have received a copy of the GNU Affero General Public License
;;;; along with CL-HSSG  If not, see <https://www.gnu.org/licenses/>.
(in-package #:hssg.blog.artifacts.blog)

(defun derive-month-period (month-period year blog)
  (with-slots ((month hssg.blog.period:month))
      month-period
    (let ((artifact (make-instance 'hssg.blog.artifacts:index-page-artifact
                      :url (list (format nil "~D" year)
                                 (format nil "~2,'0D" month))
                      :posts (hssg.blog.period:period-posts month-period)
                      :number 1
                      :total 1
                      :blog blog)))
      (hssg:derive-artifact artifact))))

(defun derive-year-period (year-period blog)
  (with-slots ((year hssg.blog.period:year)
               (months hssg.blog.period:months))
      year-period
    (let ((year-artifact (make-instance 'hssg.blog.artifacts:index-page-artifact
                           :url (list (format nil "~D" year))
                           :posts (hssg.blog.period:period-posts year-period)
                           :number 1
                           :total 1
                           :blog blog)))
      (make-instance 'hssg:compound-instruction
        :instructions (list (hssg:derive-artifact year-artifact)
                            (make-instance 'hssg:compound-instruction
                              :instructions (mapcar
                                              (lambda (month) (derive-month-period month year blog))
                                              months)))))))

(defmethod hssg:derive-artifact ((blog blog-artifact))
  (with-slots (posts categories tags periods static) blog
    (with-slots (items) categories
      (setf items (sort items #'string<= :key #'hssg.blog.artifacts:category-name)))
    (with-slots (items) tags
      (setf items (sort items #'>= :key (lambda (tag) (length (slot-value tag 'hssg.blog.artifacts:posts))))))

    (let ((artifacts (list
                       static
                       categories
                       tags
                       (make-instance 'hssg.blog.artifacts:archive-page-artifact :periods periods :blog blog)
                       (make-instance 'hssg.blog.artifacts:rss-feed-artifact :posts posts :blog blog))))
      (make-instance 'hssg:compound-instruction
        :instructions
        (concatenate
          'list
          (mapcar (lambda (year-period) (derive-year-period year-period blog)) periods)
          (mapcar #'hssg:derive-artifact posts)
          (mapcar #'hssg:derive-artifact (hssg.blog.artifact.util:collect-index-pages posts blog '()))
          (mapcar #'hssg:derive-artifact artifacts))))))
